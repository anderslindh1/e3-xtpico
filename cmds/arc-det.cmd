# RF Arc Detector ICSHWI-4079

require xtpico

# Prefix for all records
epicsEnvSet("PREFIX",           "LAB-010:RFS-ADS-11001")
epicsEnvSet("DEVICE_IP",        "172.30.5.113")

epicsEnvSet("I2C_COMM_PORT",    "AK_I2C_COMM")

#- Create the asyn port to talk XTpico server on TCP port 1002.
drvAsynIPPortConfigure($(I2C_COMM_PORT),"$(DEVICE_IP):1002")
#- asynSetTraceIOMask("$(I2C_COMM_PORT)",0,255)
#- asynSetTraceMask("$(I2C_COMM_PORT)",0,255)

# Pulse width threshold
iocshLoad("$(xtpico_DIR)/ad527x.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, NAME=:Res1, COUNT=1, INFOS=0x2F")

# Comparator level threshold
iocshLoad("$(xtpico_DIR)/ad527x.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=2, NAME=:Res2, COUNT=1, INFOS=0x2C")

# PMT Gain
iocshLoad("$(xtpico_DIR)/ad527x.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=3, NAME=:Res3, COUNT=1, INFOS=0x2E")

# PMT temperature sensor
iocshLoad("$(xtpico_DIR)/tmp100.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, NAME=:Temp, COUNT=1, INFOS=0x49")

# Voltage and current monitor
iocshLoad("$(xtpico_DIR)/ltc2991.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, NAME=:VIMon, COUNT=1, INFOS=0x48")

# I2C port extender
iocshLoad("$(xtpico_DIR)/tca9555.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, NAME=:IOExp, COUNT=1, INFOS=0x21")

# one EEPROM
iocshLoad("$(xtpico_DIR)/m24m02.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, NAME=:Eeprom, COUNT=1, INFOS=0x50")

